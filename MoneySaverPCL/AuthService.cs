﻿using MoneySaverPCL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MoneySaverPCL
{
    public class AuthService
    {
        async public Task<ApiResponse<Usuario>> Autenticar(String login, String senha)
        {

            var client = new HttpClient();

            var body = new FormUrlEncodedContent(new Dictionary<String, String>
            {
                { "login", login },
                { "senha", senha }

            }.ToList());

            var res = await client.PostAsync("http://ec2-52-67-81-18.sa-east-1.compute.amazonaws.com:8842/api/auth", body);

            if (!res.IsSuccessStatusCode)
            {

                return new ApiResponse<Usuario>
                {
                    Success = false,
                    Message = "Erro desconhecido"
                };

            }

            var json = await res.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<ApiResponse<Usuario>>(json);

        }
    }
}
