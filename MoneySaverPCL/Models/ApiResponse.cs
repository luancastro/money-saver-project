﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneySaverPCL.Models
{
    public class ApiResponse<TData>
    {
        public bool Success { set; get; }
        public String Message { set; get; }
        public TData Data { set; get; }
    }
}
