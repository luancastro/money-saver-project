﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneySaverPCL.Models
{
    public class Usuario
    {
        [Newtonsoft.Json.JsonProperty("_id")]
        public String Id { set; get; }
        public String Login { set; get; }
        public String Saldo { set; get; }
    }
}
