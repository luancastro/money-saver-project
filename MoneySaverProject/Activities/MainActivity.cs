﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using MoneySaverProject.Activities;
using Android;
using MoneySaverPCL;
using MoneySaverProject.Activities;

[assembly: Application(Theme =  "@style/AppTheme")]
namespace MoneySaverProject
{
    [Activity(Label = "MoneySaverProject", Theme = "@style/LoginTheme", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : AppCompatActivity
    {
        

        protected override void OnCreate(Bundle bundle)
        {

            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Login);

            var button = FindViewById<Button>(Resource.Id.btn_acessar);

            button.Click += Autenticar;

        }

        private async void Autenticar(object sender, System.EventArgs e)
        {

            var login = FindViewById<EditText>(Resource.Id.tv_login);

            var password = FindViewById<EditText>(Resource.Id.tv_senha);

            var auth = new AuthService();

            var retornoLogin = await auth.Autenticar(login.Text, password.Text);

            if (!retornoLogin.Success)
            {
                Toast.MakeText(this, retornoLogin.Message, ToastLength.Short).Show();
                return;
            }


            Android.Content.Intent intent = new Android.Content.Intent(this, typeof(SaldoActivity));
            intent.PutExtra("id", retornoLogin.Data.Id);
            StartActivity(intent);

        }
    }
}

