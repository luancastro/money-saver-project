using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.App;
using Android.Support.V7.App;

namespace MoneySaverProject.Activities
{
    [Activity(Label = "SaldoActivity")]
    public class SaldoActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        { 
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Home);

            var buttonEntrada = FindViewById<ImageButton>(Resource.Id.btn_entrada);
            var buttonSaida = FindViewById<ImageButton>(Resource.Id.btn_saida);

            buttonEntrada.Click += DepositoSaldo;

            buttonSaida.Click += RetiradaSaldo;

        }

        private void RetiradaSaldo(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void DepositoSaldo(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }
    }
}